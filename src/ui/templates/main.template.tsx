import React from 'react';
import styled from 'styled-components';

const GridWrapper = styled('div')`
  display: grid;
  grid-template-areas: 
    "header header"
    "aside main";
  grid-template-rows: 60px 1fr;
  grid-template-columns: 200px 1fr;
  min-height: 100vh;
  background-color: rgb(248, 249, 253);
`;

const Header = styled('header')`
  grid-area: header;
  border-bottom: 1px solid rgb(236, 240, 255);
`;

const Aside = styled('aside')`
  grid-area: aside;
  border-right: 1px solid rgb(236, 240, 255);
`;

const Main = styled('main')`
  grid-area: main;
`;

export const MainTemplate: React.FC<IMainTemplateProps> = ({header, aside, main}) => (
    <GridWrapper>
        <Header>{header}</Header>
        <Aside>{aside}</Aside>
        <Main>{main}</Main>
    </GridWrapper>
);

interface IMainTemplateProps {
    header: JSX.Element,
    aside: JSX.Element,
    main: JSX.Element,
}
