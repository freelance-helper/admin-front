import React, {ChangeEvent} from 'react';
import styled from 'styled-components';

const InputWrapper = styled('div')`
  display: flex;
  flex-direction: column;
`;

export const TextInputField: React.FC<ITextInputFieldProps> = ({name, id = name, value, label, onChange}) => {
    const handleChange = (event: ChangeEvent<HTMLInputElement>) => onChange(event.target.value, name);

    return (
        <InputWrapper>
            <label htmlFor={id}>{label}</label>
            <input
                type="text"
                id={id}
                name={name}
                value={value}
                onChange={handleChange}
            />
        </InputWrapper>
    )
};

interface ITextInputFieldProps {
    name: string,
    id?: string,
    label: string,
    value: string,
    onChange: (value: string, name: string) => void
}
