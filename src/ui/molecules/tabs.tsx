import React from 'react';
import styled from 'styled-components';
import {palette} from "../theme";

const css = String.raw;

const TabsContainer = styled('ul')`
  display: flex;
  padding: 0;
  list-style: none;
`;

const TabItem = styled('li')<{active: boolean}>`
  padding: 10px 20px;
  border: 1px solid ${palette.grey80};
  cursor: pointer;
  
  &:last-child {
    border-top-right-radius: 5px;
  }
  
  &:first-child {
    border-top-left-radius: 5px;
  }
  
  ${p => p.active && css`
    background-color: ${palette.purple};
    border-color: ${palette.purple}; 
    color: white;
  `}
`;

export const Tabs: React.FC<ITabsProps> = ({tabs, onChange, active}) => (
    <TabsContainer>
        {tabs.map(tab => (
            <TabItem
                key={tab.key}
                active={active === tab.key}
                onClick={() => onChange(tab.key)}
            >
                {tab.title}
            </TabItem>
        ))}
    </TabsContainer>
);

interface ITabsProps {
    tabs: Tab[],
    onChange: (key: string) => void,
    active: string;
}

interface Tab {
    title: string,
    key: string;
}
