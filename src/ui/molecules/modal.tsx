import React from 'react';
import styled from 'styled-components';

const Wrapper = styled('div')<{open: boolean}>`
  position: fixed;
  display: ${p => p.open ? 'block' : 'none'};
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
`;

const Overlay = styled('div')`
  background-color: rgba(0, 0, 0, .2);
  width: 100%;
  height: 100%;
`;

const Content = styled('div')`
  position: absolute;
  left: 50%;
  top: 50%;
  min-width: 500px;
  transform: translate(-50%, -50%);
  background-color: white;
  padding: 30px;
`;

export const Modal: React.FC<IModalProps> = ({children, open, onClose}) => {
    return (
        <Wrapper open={open}>
            <Overlay onClick={() => onClose()}/>
            <Content>
                {children}
            </Content>
        </Wrapper>
    )
};

interface IModalProps {
    open: boolean,
    onClose: Function
}
