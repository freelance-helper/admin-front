import React from 'react';
import styled from 'styled-components';
import { Link } from "react-router-dom";

const Wrapper = styled('nav')`
  padding: 10px;
`;

const List = styled('ul')`
  list-style: none;
`;

const Item = styled('li')`
  
`;

export const Navigation: React.FC<INavigationProps> = ({items}) => (
    <Wrapper>
        <List>
            {items.map(item => (
                <Item key={item.key}>
                    <Link to={item.to}>{item.title}</Link>
                </Item>
            ))}
        </List>
    </Wrapper>
);

interface INavigationProps {
    items: INavigationItem[]
}

interface INavigationItem {
    title: string,
    key: string,
    to: string
}
