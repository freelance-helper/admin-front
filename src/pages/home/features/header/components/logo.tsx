import React from 'react';
import styled from 'styled-components';

import logoImage from '../assets/logo.svg';

const LogoContainer = styled('div')`
  max-width: 30px;
  
  img {
    width: 100%;
  }
`;

export const Logo: React.FC = () => (
    <LogoContainer>
        <img src={logoImage} alt="Logo"/>
    </LogoContainer>
);
