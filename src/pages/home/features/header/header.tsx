import React from 'react';
import styled from 'styled-components';

import {Logo} from './components/logo';

const Wrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
`;

export const Header: React.FC = () => (
    <Wrapper>
        <Logo/>
    </Wrapper>
);
