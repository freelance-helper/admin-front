import React from 'react';
import { Switch, Route } from "react-router-dom";

import {ExchangesPage, exchanges} from '../../pages/exchanges';
import {ExchangeDetailsPage, exchangeDetails} from '../../pages/exchange-details';

export const Main: React.FC = () => (
    <Switch>
        <Route path={exchanges.path} component={ExchangesPage}/>
        <Route path={exchangeDetails.path} component={ExchangeDetailsPage}/>
    </Switch>
);
