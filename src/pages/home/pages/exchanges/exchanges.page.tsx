import React from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import {exchangeDetails} from '../exchange-details';

import {ColumnTypes, CRUDTable} from 'features/crud-table';
import {ITableColumns} from 'features/crud-table/crud-table.types';
import {createExchange, deleteExchange, exchangesListSelector, fetchExchanges, updateExchange} from './store';
import {IExchange} from './store/types';
import {RouteComponentProps} from "react-router";

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  padding: 10px;
`;

const Title = styled('h2')`
  font-weight: 600;
`;

const mapStateToProps = (state: any) => ({
    exchanges: exchangesListSelector(state)
});

const mapDispatchToProps = {
    fetchExchanges,
    updateExchange,
    createExchange,
    deleteExchange
};

export const ExchangesPage = connect(mapStateToProps, mapDispatchToProps)(
    class ExchangesPageView extends React.Component<IExchangesPageProps> {
        componentDidMount() {
            this.props.fetchExchanges()
        }

        handleCellClick = (exchange: IExchange) =>  {
            this.props.history.push(exchangeDetails.getPath(String(exchange.exchangeId)));
        };

        render() {
            return (
                <Wrapper>
                    <Title>Биржи</Title>
                    <CRUDTable
                        columns={columns}
                        items={this.props.exchanges}
                        getId={(exchange: IExchange) => exchange.exchangeId}
                        createItem={this.props.createExchange}
                        updateItem={this.props.updateExchange}
                        deleteItem={this.props.deleteExchange}
                        onCellClick={this.handleCellClick}
                    />
                </Wrapper>
            )
        }
    }
);

const columns: ITableColumns = {
    exchangeId: {
        size: '80px',
        name: 'ИД',
        editable: false,
        cast: Number,
        type: ColumnTypes.TEXT,
    },
    name: {
        size: '160px',
        name: 'Название',
        editable: true,
        clickable: true,
        cast: String,
        type: ColumnTypes.TEXT,
    },
    selector: {
        size: '1fr',
        name: 'Селектор',
        editable: true,
        cast: String,
        type: ColumnTypes.TEXT,
    },
    url: {
        size: '1fr',
        name: 'Ссылка',
        editable: true,
        cast: String,
        type: ColumnTypes.TEXT,
    }
};

interface IExchangesPageProps extends RouteComponentProps {
    fetchExchanges: Function,
    createExchange: Function,
    deleteExchange: Function,
    updateExchange: (exchange: IExchange) => Promise<void>
    exchanges: IExchange[]
}
