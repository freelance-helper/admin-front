import {LoadingStatus} from 'store/loading-status';
import {FETCH_EXCHANGES, CREATE_EXCHANGE, UPDATE_EXCHANGE, DELETE_EXCHANGE, IExchange} from './types';

const initialState: IExchangesStore = {
    list: [],
    status: LoadingStatus.INITIAL,
};

export const exchangesReducer = (state = initialState, {type, payload}: IAction) => {
    switch (type) {
        case FETCH_EXCHANGES:
            return {...state, list: payload};
        case CREATE_EXCHANGE:
            return {...state, list: [...state.list, payload]};
        case DELETE_EXCHANGE:
            return {...state, list: state.list.filter(exchange => exchange.exchangeId !== payload)};
        case UPDATE_EXCHANGE:
            return {
                ...state,
                list: state.list
                    .map(
                        (exchange: IExchange) => exchange.exchangeId === payload.exchangeId ? payload : exchange
                    )
            };
        default:
            return state;
    }
};

export interface IExchangesStore {
    list: Array<any>,
    status: LoadingStatus
}

export interface IAction {
    type: string;
    payload: any;
}
