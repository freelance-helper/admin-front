import {Dispatch} from "redux";
import {Api} from 'api';
import {FETCH_EXCHANGES, UPDATE_EXCHANGE, CREATE_EXCHANGE, DELETE_EXCHANGE, IExchange, IExchangeDTO} from './types';

const EXCHANGES_URL = '/exchanges';

export const fetchExchanges = () => async (dispatch: Dispatch) => {
    const res = await Api.get<IExchange[]>(EXCHANGES_URL);

    dispatch({
        type: FETCH_EXCHANGES,
        payload: res.data
    })
};

export const updateExchange = (exchange: IExchange) => async (dispatch: Dispatch) => {
    const res = await Api.put(EXCHANGES_URL, exchange);

    dispatch({
        type: UPDATE_EXCHANGE,
        payload: res.data,
    })
};

export const createExchange = (exchange: IExchangeDTO) => async (dispatch: Dispatch) => {
    const res = await Api.post<IExchange>(EXCHANGES_URL, exchange);

    dispatch({
        type: CREATE_EXCHANGE,
        payload: res.data,
    })
};

export const deleteExchange = (exchange: IExchange) => async (dispatch: Dispatch) => {
    await Api.delete(`${EXCHANGES_URL}/${exchange.exchangeId}`);

    dispatch({
        type: DELETE_EXCHANGE,
        payload: exchange.exchangeId
    })
};
