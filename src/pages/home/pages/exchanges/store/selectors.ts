export const exchangesSelector = (state: any) => state.exchanges;
export const exchangesListSelector = (state: any) => exchangesSelector(state).list || [];
export const exchangesStatusSelector = (state: any) => exchangesSelector(state).status;
