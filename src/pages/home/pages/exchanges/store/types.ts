export const FETCH_EXCHANGES = 'FETCH_EXCHANGES';
export const UPDATE_EXCHANGE = 'UPDATE_EXCHANGE';
export const CREATE_EXCHANGE = 'CREATE_EXCHANGE';
export const DELETE_EXCHANGE = 'DELETE_EXCHANGE';

export interface IExchange {
    [key:string]: any,
    exchangeId: number,
    name: string,
    selector: string,
    url: string,
}

export interface IExchangeDTO {
    name: string,
    selector: string,
    url: string,
}
