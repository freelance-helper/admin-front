import {combineReducers} from 'redux';

import {categoriesReducer} from './pages/categories';
import {targetsReducer} from './pages/targets';

export const exchangeDetailsReducer = combineReducers({
    categories: categoriesReducer,
    targets: targetsReducer,
});
