import React, {useState} from 'react';
import styled from 'styled-components';
import {Route, RouteComponentProps} from "react-router";

import {Tabs} from 'ui';
import {categories, CategoriesPage} from './pages/categories';
import {targets, TargetsPage} from './pages/targets';

const Wrapper = styled('div')`
  padding: 10px;
`;

const Container = styled('div')``;

const links: {[key: string]: {getPath: Function, path: string}} = {
    categories,
    targets,
};

export const ExchangeDetailsPage: React.FC<RouteComponentProps<{id: string}>> = ({match, history}) => {
    const [active, setActive] = useState('categories');
    const exchangeId = match.params.id;

    const handleTabChange = (key: string) => {
        setActive(key);
        history.push(links[key].getPath(match.params.id))
    };

    return (
        <Wrapper>
            <Tabs tabs={tabs} active={active} onChange={handleTabChange}/>
            <Container>
                <Route
                    path={categories.getPath(exchangeId)}
                    component={(props: any) => <CategoriesPage {...props} exchangeId={exchangeId}/>}
                />
                <Route
                    path={targets.getPath(exchangeId)}
                    component={(props: any) => <TargetsPage {...props} exchangeId={Number(exchangeId)}/>}
                />
            </Container>
        </Wrapper>
    )
};

const tabs = [
    {title: 'Категории', key: 'categories'},
    {title: 'Таргеты', key: 'targets'},
    {title: 'Шаблоны', key: 'templates'},
];
