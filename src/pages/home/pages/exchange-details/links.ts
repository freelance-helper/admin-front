export const exchangeDetails = {
    path: '/home/exchange-details/:id',
    getPath: (id: string | number) => `/home/exchange-details/${id}`
};
