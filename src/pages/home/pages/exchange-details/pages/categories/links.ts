export const categories = {
    path: '/home/exchange-details/:id/categories',
    getPath: (id: string) => `/home/exchange-details/${id}/categories`,
};
