import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {RouteComponentProps} from "react-router";

import {Modal} from 'ui';
import {CategoriesTree, CategoryForm} from './components';
import {ICategory} from './store/reducer';
import {ICategoryDTO, ICategoryItem, ICategoryUpdateDTO} from './categories.page.types';
import {
    categoriesSelector,
    fetchCategories,
    createCategory,
    updateCategory,
    deleteCategory,
} from './store';

const addParentToChild = (parent?: number) => (category: ICategory): ICategoryItem => {
    return {
        ...category,
        children: formatCategories(category.children, category.id),
        parent,
    }
};

const formatCategories = (categories: ICategory[], parent?: number): ICategoryItem[] => {
    return categories.map(addParentToChild(parent))
};

const mapStateToProps = (state: any) => ({
    categories: formatCategories(categoriesSelector(state)),
});

const mapDispatchToProps = {
    fetchCategories,
    createCategory,
    deleteCategory,
    updateCategory,
};

export const CategoriesPage = connect(mapStateToProps, mapDispatchToProps)(
    class CategoriesView extends React.Component<ICategoriesProps, ICategoriesState> {
        state = {
            newCategoryParent: null,
            editCategory: null,
        };

        componentDidMount() {
            this.props.fetchCategories(Number(this.props.exchangeId))
        }

        handleNewCategory = (parentId?: number) => {
            this.setState({newCategoryParent: parentId})
        };

        handleEditCategory = (category: ICategoryItem) => {
            this.setState({editCategory: category})
        };

        handleEditModalClose = () => {
            this.setState({editCategory: null})
        };

        handleNewCategoryClose = () => this.setState({newCategoryParent: null});

        handleCreateCategory = (name: string, url: string) => {
            if (this.state.newCategoryParent !== null) {
                this.props.createCategory({
                    name,
                    url,
                    parentId: (this.state as any).newCategoryParent,
                    exchangeId: Number(this.props.exchangeId)
                });
                this.setState({
                    newCategoryParent: null
                });
            }
        };

        handleUpdateCategory = async (name: string, url: string) => {
            const category = {
                id: (this.state as any).editCategory.id,
                name,
                url
            };

            await this.props.updateCategory(category, Number(this.props.exchangeId));
            this.handleEditModalClose();
        };

        handleDeleteCategory = (categoryId: number) => {
            this.props.deleteCategory(categoryId, Number(this.props.exchangeId));
        };

        render() {
            return (
                <Fragment>
                    <CategoriesTree
                        categories={this.props.categories}
                        onNewCategory={this.handleNewCategory}
                        onEditCategory={this.handleEditCategory}
                        onDeleteCategory={this.handleDeleteCategory}
                    />
                    <Modal
                        open={this.state.newCategoryParent !== null}
                        onClose={this.handleNewCategoryClose}
                    >
                        <h2>Новая категория</h2>
                        <CategoryForm name="" url="" onSuccess={this.handleCreateCategory}/>
                    </Modal>
                    <Modal
                        open={this.state.editCategory !== null}
                        onClose={this.handleEditModalClose}
                    >
                        {this.state.editCategory && (
                            <Fragment>
                                <h2>{(this.state as any).editCategory.name}</h2>
                                <CategoryForm
                                    name={(this.state as any).editCategory.name}
                                    url={(this.state as any).editCategory.url}
                                    onSuccess={this.handleUpdateCategory}
                                />
                            </Fragment>
                        )}
                    </Modal>
                </Fragment>
            )
        }
    }
);

interface ICategoriesProps extends RouteComponentProps<{ id: string }> {
    fetchCategories: (exchangeId: number) => void;
    updateCategory: (category: ICategoryUpdateDTO, exchangeId: number) => void;
    createCategory: (category: ICategoryDTO) => void;
    deleteCategory: (categoryId: number, exchangeId: number) => void;
    categories: ICategoryItem[],
    exchangeId: number
}

interface ICategoriesState {
    newCategoryParent: number | undefined | null;
    editCategory: ICategoryItem | null;
}
