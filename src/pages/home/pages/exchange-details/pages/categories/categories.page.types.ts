import {ICategory} from "./store/reducer";

export interface ICategoryItem extends ICategory {
    parent?: number;
}

export interface ICategoryDTO {
    name: string;
    url: string;
    exchangeId: number;
    parentId: number;
}

export interface ICategoryUpdateDTO {
    id: number;
    name: string;
    url: string;
}
