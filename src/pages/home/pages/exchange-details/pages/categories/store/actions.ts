import {Dispatch} from "redux";
import {Api} from "api";
import {ICategoryDTO, ICategoryUpdateDTO} from '../categories.page.types';
import {axios} from "libs/axios";

export const FETCH_CATEGORIES = 'FETCH_CATEGORIES';

const getCategories = (exchangeId: number) => Api.get(`/categories/${exchangeId}/tree`);

export const fetchCategories = (exchangeId: number) => async (dispatch: Dispatch) => {
    const res = await getCategories(exchangeId);

    dispatch({
        type: FETCH_CATEGORIES,
        payload: res.data
    })
};

export const createCategory = (category: ICategoryDTO) => async (dispatch: Dispatch) => {
    await Api.post('/categories', category);
    const res = await getCategories(category.exchangeId);

    dispatch({
        type: FETCH_CATEGORIES,
        payload: res.data,
    })
};

export const deleteCategory = (categoryId: number, exchangeId: number) => async (dispatch: Dispatch) => {
    await Api.delete(`/categories/${categoryId}`);
    const res = await getCategories(exchangeId);

    dispatch({
        type: FETCH_CATEGORIES,
        payload: res.data,
    });
};

export const updateCategory = (category: ICategoryUpdateDTO, exchangeId: number) => async (dispatch: Dispatch) => {
    await axios.put('/categories', category);
    const res = await getCategories(exchangeId);

    dispatch({
        type: FETCH_CATEGORIES,
        payload: res.data,
    });
};
