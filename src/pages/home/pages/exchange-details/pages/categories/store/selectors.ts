export const categoriesRootSelector = (state: any) => state.exchangeDetails.categories;
export const categoriesSelector = (state: any) => categoriesRootSelector(state).list;
