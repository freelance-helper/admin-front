import {FETCH_CATEGORIES} from './actions';

export interface ICategoriesStore {
    list: ICategory[]
}

export interface ICategory {
    id: number;
    name: string;
    url: string;
    children: ICategory[];
}

const initialState: ICategoriesStore = {
    list: []
};

export const categoriesReducer = (state = initialState, {type, payload}: {type: string, payload: any}) => {
    switch (type) {
        case FETCH_CATEGORIES: return {...state, list: payload};
        default: return state;
    }
};
