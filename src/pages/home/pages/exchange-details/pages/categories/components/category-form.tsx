import React, {Fragment, useState} from 'react';
import styled from "styled-components";

import {palette} from "ui/theme";
import {TextInputField} from "ui/molecules";

const InputWrapper = styled('div')`
  margin: 10px 0;
`;

const AddButton = styled('button')`
  display: block;
  padding: 10px 30px;
  margin: 20px auto 0;
  background-color: ${palette.purple};
  color: white;
  border-radius: 5px;
  cursor: pointer;
`;

export const CategoryForm: React.FC<ICategoryFormProps> = ({onSuccess, name, url}) => {
    const [inputName, setName] = useState(name);
    const [inputUrl, setUrl] = useState(url);

    const handleChange = (setValue: Function) => (value: any) => setValue(value);
    const success = () => {
        onSuccess(inputName, inputUrl);
        setName('');
        setUrl('');
    };

    return (
        <Fragment>
            <InputWrapper>
                <TextInputField label="Название" name='name' onChange={handleChange(setName)} value={inputName}/>
            </InputWrapper>
            <InputWrapper>
                <TextInputField label="Ссылка" name="url" onChange={handleChange(setUrl)} value={inputUrl}/>
            </InputWrapper>
            <AddButton onClick={success}>Добавить категорию</AddButton>
        </Fragment>
    )
};

interface ICategoryFormProps {
    onSuccess: (name: string, url: string) => void;
    name: string;
    url: string;
}
