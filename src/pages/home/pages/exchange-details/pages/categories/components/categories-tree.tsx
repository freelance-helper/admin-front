import React, {useState} from 'react';
import styled from 'styled-components';

import {palette} from "ui/theme";
import {ICategoryItem} from '../categories.page.types';
import cancelIcon from "../assets/cancel.svg";
import rightArrow from "../assets/right-arrow.svg";
import addIcon from "../assets/add.svg";
import editIcon from "../assets/pencil-edit-button.svg";

const css = String.raw;

const CategoriesContainer = styled('div')`
  display: flex;
`;

const CategoriesColumn = styled('ul')`
  list-style: none;
  padding: 0;
  margin: 0;
`;

const CategoryButton = styled('button')`
  margin-left: 15px;
  border: none;
  width: 1em;
  height: 1em;
  background: url("${cancelIcon}") center center no-repeat / cover;
  cursor: pointer;
  transition: all .2s ease-in-out;
  opacity: 0;
`;

const CategoryEdit = styled(CategoryButton)`
  background: url("${editIcon}") center center no-repeat / cover;
`;

const CategoriesItem = styled('li')<{ active?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 10px;
  margin-bottom: 10px;
  margin-right: 10px;
  border: 1px solid ${palette.grey80};
  border-radius: 5px;
  cursor: pointer;
  transition: all .2s ease-in-out;
  background-color: white;
  
  &:after {
    content: '';
    display: block;
    width: 1em;
    height: 1em;
    margin-left: 5px;
    background: url("${rightArrow}") center center no-repeat / cover;
  }
  
  &:hover {
    background-color: ${palette.softBlue20};
    
    ${CategoryButton}, ${CategoryEdit} {
      opacity: 1;
    }
  }
  
  ${p => p.active && css`
    color: white; 
    background-color: ${palette.purple}; 
    border-color: ${palette.purple};
    &:hover {
      background-color: ${palette.softPurple};
    }
  `};
`;

const CategoriesAdd = styled(CategoriesItem)`
  font-style: italic;
  text-decoration: underline;
  border: none;
  background-color: transparent;
  
  &:after {
    background: url("${addIcon}") center center no-repeat / cover;
  }
`;

const CategoryName = styled('span')`
  margin-right: auto;
`;

const getPath = (categories: ICategoryItem[], nextCategory: ICategoryItem, path: ICategoryItem[]): ICategoryItem[] => {
    if (!nextCategory.parent) {
        return [nextCategory];
    }

    const index = path
        .map(item => item.parent)
        .filter(item => !!item)
        .indexOf(nextCategory.parent);

    if (index < 0) {
        return [...path, nextCategory];
    } else {
        return [...path.slice(0, index + 1), nextCategory];
    }
};

const categoriesToFlat = (categories: ICategoryItem[]): ICategoryItem[] => {
    return categories.reduce((acc: ICategoryItem[], item: ICategoryItem) => [...acc, item, ...categoriesToFlat(item.children)], [])
};

const getUpdatedPath = (categories: ICategoryItem[], path: ICategoryItem[]) => {
    const flatCategories = categoriesToFlat(categories);

    return path
        .map(item => flatCategories.find(category => item.id === category.id))
        .filter(item => item !== undefined);
};

const isActive = (path: ICategoryItem[], category: ICategoryItem) => {
    return path.map(item => item.id).indexOf(category.id) >= 0
};

export const CategoriesTree: React.FC<ICategoriesTreeProps> = (
    {categories, onNewCategory, onDeleteCategory, onEditCategory}
) => {
    const [path, setPath] = useState<ICategoryItem[]>([]);

    const handleCategoryClick = (category: ICategoryItem) => () => {
        setPath(getPath(categories, category, path))
    };

    const handleEditCategory = (category: ICategoryItem) => () => onEditCategory(category);

    const handleDeleteCategory = (categoryId: number) => () => {
        setPath([]);
        onDeleteCategory(categoryId);
    };

    const updatedPath = (getUpdatedPath(categories, path) as ICategoryItem[]);

    return (
        <CategoriesContainer>
            <CategoriesColumn>
                {categories.map(item => (
                    <CategoriesItem
                        active={isActive(path, item)}
                        onClick={handleCategoryClick(item)}
                        key={item.id}
                    >
                        <CategoryName>
                            {item.name}
                        </CategoryName>
                        <CategoryEdit onClick={handleEditCategory(item)}/>
                        <CategoryButton onClick={handleDeleteCategory(item.id)}/>
                    </CategoriesItem>
                ))}
                <CategoriesAdd onClick={() => onNewCategory()}>
                    Добавить категорию
                </CategoriesAdd>
            </CategoriesColumn>
            {updatedPath.map((category: ICategoryItem, index: number) => (
                <CategoriesColumn key={index}>
                    {category.children.map((category: ICategoryItem) => (
                        <CategoriesItem
                            active={isActive(path, category)}
                            onClick={handleCategoryClick(category)}
                            key={category.id}
                        >
                            <CategoryName>
                                {category.name}
                            </CategoryName>
                            <CategoryEdit onClick={handleEditCategory(category)}/>
                            <CategoryButton onClick={handleDeleteCategory(category.id)}/>
                        </CategoriesItem>
                    ))}
                    <CategoriesAdd onClick={() => onNewCategory(category.id)}>
                        Добавить категорию
                    </CategoriesAdd>
                </CategoriesColumn>
            ))}
        </CategoriesContainer>
    )
};

interface ICategoriesTreeProps {
    categories: ICategoryItem[];
    onNewCategory: (parent?: number) => void;
    onEditCategory: (category: ICategoryItem) => void
    onDeleteCategory: (categoryId: number) => void;
}
