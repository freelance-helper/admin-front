export interface ITarget {
    id: number;
    selector: string;
    type: string;
    isOrderLink: boolean;
}
