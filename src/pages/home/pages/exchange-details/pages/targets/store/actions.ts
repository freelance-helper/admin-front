import {Api} from "api";

export const FETCH_TARGETS = 'FETCH_TARGETS';
export const CREATE_TARGET = 'CREATE_TARGET';
export const DELETE_TARGET = 'DELETE_TARGET';
export const UPDATE_TARGET = 'UPDATE_TARGET';
const TARGETS_URL = '/orders-configs';

export const fetchTargets = (exchangeId: number) => async (dispatch: any) => {
    const res = await Api.get(`${TARGETS_URL}?exchange=${exchangeId}`);

    dispatch({
        type: FETCH_TARGETS,
        payload: res.data
    })
};

export const createTarget = (target: any) => async (dispatch: any) => {
    const res = await Api.post(TARGETS_URL, target);

    dispatch({
        type: CREATE_TARGET,
        payload: res.data,
    })
};

export const deleteTarget = (id: number) => async (dispatch: any) => {
    await Api.delete(`${TARGETS_URL}/${id}`);

    dispatch({
        type: DELETE_TARGET,
        payload: id,
    })
};

export const updateTarget = (target: any) => async (dispatch: any) => {
    const res = await Api.put(TARGETS_URL, target);

    dispatch({
        type: UPDATE_TARGET,
        payload: res.data,
    })
};
