import {FETCH_TARGETS, CREATE_TARGET, DELETE_TARGET, UPDATE_TARGET} from './actions';
import {ITarget} from './types';

const initialState: Array<ITarget> = [];

export const targetsReducer = (state = initialState, {type, payload}: {type: string, payload: any}) => {
    switch (type) {
        case FETCH_TARGETS: return payload;
        case CREATE_TARGET: return [...state, payload];
        case UPDATE_TARGET: return state.map(item => item.id === payload.id ? payload : item);
        case DELETE_TARGET: return state.filter(item => item.id !== payload);
        default: return state;
    }
};
