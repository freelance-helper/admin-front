import React from 'react';
import {connect} from 'react-redux';

import {CRUDTable, ColumnTypes} from 'features/crud-table';
import {ITableColumns} from 'features/crud-table/crud-table.types';
import {targetsListSelector, fetchTargets, createTarget, updateTarget, deleteTarget} from './store';
import {ITarget} from './store/types';

const mapStateToProps = (state: any) => ({
    targets: targetsListSelector(state),
});

const mapDispatchToProps = {
    fetchTargets,
    createTarget,
    deleteTarget,
    updateTarget,
};

export const TargetsPage = connect(mapStateToProps, mapDispatchToProps)(
    class TargetsView extends React.Component<ITargetsPageProps> {
        componentDidMount() {
            this.props.fetchTargets(this.props.exchangeId);
        }

        render() {
            const {targets} = this.props;

            return (
                <CRUDTable
                    columns={columns}
                    items={targets || []}
                    getId={(target: ITarget) => target.id}
                    updateItem={this.props.updateTarget}
                    deleteItem={(target: ITarget) => this.props.deleteTarget(target.id)}
                    createItem={this.props.createTarget}
                />
            )
        }
    }
);

interface ITargetsPageProps {
    targets: ITarget[];
    exchangeId: number;
    fetchTargets: (exchangeId: number) => void;
    createTarget: (order: any) => void;
    updateTarget: (order: any) => void;
    deleteTarget: (id: number) => void;
}

const columns: ITableColumns = {
    id: {
        size: '50px',
        name: 'ИД',
        editable: false,
        type: ColumnTypes.TEXT,
        cast: Number,
    },
    type: {
        size: '100px',
        name: 'Тип',
        editable: true,
        type: ColumnTypes.SELECT,
        cast: String,
        select: [
            {title: 'text', value: 'text'},
            {title: 'href', value: 'href'},
        ],
    },
    isOrderLink: {
        size: '100px',
        name: 'Это ссылка',
        editable: true,
        type: ColumnTypes.CHECKBOX,
        cast: Boolean,
    },
    selector: {
        size: '1fr',
        name: 'Селектор',
        editable: true,
        type: ColumnTypes.TEXT,
        cast: String,
    },
};
