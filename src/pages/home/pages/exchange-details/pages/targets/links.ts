export const targets = {
    path: '/home/exchange-details/:id/targets',
    getPath: (id: string) => `/home/exchange-details/${id}/targets`,
};
