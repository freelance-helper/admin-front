import React from 'react';

import {Header} from './features/header';
import {Navigation} from './features/navigation';
import {Main} from './features/main';

import {MainTemplate} from 'ui';
import {exchanges} from './pages/exchanges';

export const Home: React.FC = () => (
    <MainTemplate
        header={<Header/>}
        aside={<Navigation items={navigationItems}/>}
        main={<Main/>}
    />
);

const navigationItems = [
    {title: 'Биржи', key: 'exchanges', to: exchanges.path}
];
