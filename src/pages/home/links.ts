import {links} from '../../links';
import {exchanges} from './pages/exchanges';
import {exchangeDetails} from './pages/exchange-details';
import {ILink} from "react-app-env";

export const home: ILink = {
    path: links.path + 'home',
    children: {
        exchanges,
        exchangeDetails
    }
};
