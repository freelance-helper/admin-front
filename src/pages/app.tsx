import React from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';

import {Home} from 'pages/home';

const App: React.FC = () => {
    return (
        <Switch>
            <Route path="/home" component={Home}/>
            <Redirect exact from="/" to="/home"/>
        </Switch>
    );
};

export default App;
