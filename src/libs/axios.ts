import pureAxios from 'axios';
import {config} from 'config';

export const axios = pureAxios.create({
    baseURL: config.API_HOST
});
