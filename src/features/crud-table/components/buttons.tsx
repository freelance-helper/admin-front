import styled from "styled-components";
import React from "react";
import editIcon from "../../../pages/home/pages/exchanges/assets/pencil-edit-button.svg";
import checkIcon from "../../../pages/home/pages/exchanges/assets/checked.svg";
import addIcon from "../../../pages/home/pages/exchanges/assets/add.svg";
import cancelIcon from "../../../pages/home/pages/exchanges/assets/cancel.svg";

const IconButtonWrapper = styled('button')`
  display: flex;
  align-items: center;
  justify-content: center;
  background: none;
  border: none;

  img {
    width: 100%;
  }
`;

export const IconButton: React.FC<{ icon: string, onClick: any }> = ({icon, onClick}) => (
    <IconButtonWrapper onClick={onClick}>
        <img src={icon} alt="Edit"/>
    </IconButtonWrapper>
);

interface IButtonProps {
    onClick: Function
}

export const EditButton: React.FC<IButtonProps> = ({onClick}) => (
    <IconButton icon={editIcon} onClick={onClick}/>
);

export const SaveButton: React.FC<IButtonProps> = ({onClick}) => (
    <IconButton icon={checkIcon} onClick={onClick}/>
);

export const AddButton: React.FC<IButtonProps> = ({onClick}) => (
    <IconButton icon={addIcon} onClick={onClick}/>
);

export const DeleteButton: React.FC<IButtonProps> = ({onClick}) => (
    <IconButton icon={cancelIcon} onClick={onClick}/>
);
