import React from 'react';

import styled from "styled-components";
import {palette} from "ui/theme";

const TableWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  border: 1px solid ${palette.grey80};
`;

const Row = styled('div')<{ sizes: string }>`
  display: grid;
  grid-template-columns: ${p => p.sizes} 30px 30px;
  grid-template-rows: 30px;
  border-bottom: 1px solid ${palette.grey80};
  
  &:last-child {
    border-bottom: none;
  }
`;

const Cell = styled('div')<{clickable?: boolean}>`
  display: flex;
  align-items: center;
  justify-content: center;
  border-right: 1px solid ${palette.grey80};
  ${p => p.clickable && `cursor: pointer; &:hover {text-decoration: underline;}`}
  
  &:last-child {
    border-right: none;
  }
  
  input {
    width: 100%;
    height: 100%;
    box-sizing: border-box;
  }
`;

const HeaderRow = styled(Row)`
  ${Cell} {
    font-weight: 500;
  }
`;

interface ITableComponent extends React.FC {
    Row: any;
    Cell: any;
    HeaderRow: any;
}

const Table: ITableComponent = ({children}) => (
    <TableWrapper>
        {children}
    </TableWrapper>
);

Table.Row = Row;
Table.Cell = Cell;
Table.HeaderRow = HeaderRow;

export {Table};
