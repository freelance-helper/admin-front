import {ColumnTypes} from './column-types';

export interface ITableColumns {
    [key: string]: ITableColumn
}

export interface ITableColumn {
    [key: string]: string | boolean | ColumnTypes | ISelectItem[] | Function | undefined;
    size: string;
    name: string;
    editable: boolean;
    clickable?: boolean;
    cast: Function;
    type: ColumnTypes;
    select?: ISelectItem[];
}

export interface ISelectItem {
    title: string;
    value: string;
}
