import React, {Fragment} from 'react';

import {ISelectItem, ITableColumns} from './crud-table.types';
import {AddButton, DeleteButton, EditButton, SaveButton} from './components/buttons';
import {Table} from './components/table';
import {ColumnTypes} from "./column-types";

export class CRUDTable extends React.Component<ICRUDTableProps, ICRUDTableState> {
    state = {
        editableRow: null,
        newRow: this.getDefaultState(this.props.columns),
    };

    getDefaultValue(key: string, columns: any): any {
        switch (columns[key].type) {
            case ColumnTypes.SELECT:
                return (columns[key].select as any)[0].value;
            default:
                return columns[key].cast('');
        }
    };

    getDefaultState(columns: any): any {
        return Object.keys(columns).reduce((acc, key) => {
            return (columns[key] as any).editable ? {...acc, [key]: this.getDefaultValue(key, columns)} : acc;
        }, {})
    }

    getSizes() {
        const columns = this.props.columns;

        return Object.keys(columns).map(key => columns[key].size).join(' ')
    }

    getItemKeys() {
        return Object.keys(this.props.columns) as string[];
    }

    handleEdit = (item: any) => () => {
        this.setState({editableRow: item})
    };

    handleEditSave = async () => {
        if (this.state.editableRow === null) {
            return;
        }

        await this.props.updateItem(this.state.editableRow);

        this.setState({editableRow: null})
    };

    handleCellClick = (item: any) => () => {
        this.props.onCellClick && this.props.onCellClick(item)
    };

    handleChangeInput = (value: string | boolean, name: string) => {
        if (this.state.editableRow === null) {
            return;
        }

        this.setState({
            editableRow: {
                ...(this.state.editableRow as any),
                [name]: value
            }
        })
    };

    handleNewChange = (value: any, name: string) => {
        this.setState({
            newRow: {
                ...this.state.newRow,
                [name]: value
            }
        })
    };

    handleDelete = (item: any) => async () => {
        await this.props.deleteItem(item);
    };

    handleSaveClick = async () => {
        await this.props.createItem(this.state.newRow);
        this.setState({
            newRow: this.getDefaultState(this.props.columns)
        })
    };

    isColumnEditable = (key: string) => {
        return !!this.props.columns[key].editable;
    };

    isRowEditable = (item: any): boolean => {
        if (this.state.editableRow !== null) {
            return this.props.getId(this.state.editableRow) === this.props.getId(item);
        } else {
            return false;
        }
    };

    renderColumnInput = (key: string, item: any, onChange: Function) => {
        const type = this.props.columns[key].type;

        switch (type) {
            case ColumnTypes.SELECT:
                return (
                    <select name={key} key={key} value={item[key]} id={key}
                            onChange={(event) => onChange(event.target.value, key)}>
                        {(this.props.columns[key].select || []).map((option: ISelectItem) => (
                            <option key={option.value} value={option.value}>{option.title}</option>
                        ))}
                    </select>
                );
            case ColumnTypes.CHECKBOX:
                return (
                    <input
                        type="checkbox"
                        key="key"
                        checked={item[key]}
                        onChange={(event) => onChange(event.target.checked, key)}
                    />
                );
            default:
                return (
                    <input
                        key={key}
                        type="text"
                        value={item[key]}
                        onChange={(event) => onChange(event.target.value, key)}
                    />
                )
        }
    };

    renderCreateRow = () => {
        return (
            <Table.Row sizes={this.getSizes()}>
                {this.getItemKeys().map((key: string) => (
                    this.isColumnEditable(key) ? (
                        <Table.Cell key={key}>
                            {this.renderColumnInput(key, this.state.newRow, this.handleNewChange)}
                        </Table.Cell>
                    ) : (
                        <Table.Cell key={key}>&nbsp;</Table.Cell>
                    )
                ))}
                <AddButton onClick={this.handleSaveClick}/>
            </Table.Row>
        )
    };

    renderEditableRow = (item: any) => {
        return (
            <Fragment>
                {this.getItemKeys().map((key: string) => (
                    this.isColumnEditable(key) ? (
                        <Table.Cell key={key}>
                            {this.renderColumnInput(key, this.state.editableRow, this.handleChangeInput)}
                        </Table.Cell>
                    ) : (
                        <Table.Cell key={key}>{item[key]}</Table.Cell>
                    )
                ))}
                <SaveButton onClick={this.handleEditSave}/>
            </Fragment>
        )
    };

    renderRow = (item: any) => {
        return (
            <Fragment>
                {this.getItemKeys().map((key: string) => (
                    <Table.Cell
                        key={key}
                        clickable={!!this.props.columns[key].clickable}
                        onClick={
                            this.props.columns[key].clickable &&
                            this.handleCellClick(item)
                        }
                    >
                        {item[key] + ''}
                    </Table.Cell>
                ))}
                <EditButton onClick={this.handleEdit(item)}/>
                <DeleteButton onClick={this.handleDelete(item)}/>
            </Fragment>
        )
    };

    render() {
        return (
            <Table>
                <Table.HeaderRow sizes={this.getSizes()}>
                    {this.getItemKeys().map(key => (
                        <Table.Cell key={key}>{this.props.columns[key].name}</Table.Cell>
                    ))}
                </Table.HeaderRow>
                {this.props.items.map((item: any) => (
                    <Table.Row sizes={this.getSizes()} key={this.props.getId(item)}>
                        {this.isRowEditable(item) ?
                            this.renderEditableRow(item) :
                            this.renderRow(item)}
                    </Table.Row>
                ))}
                {this.renderCreateRow()}
            </Table>
        )
    }
}

export interface ICRUDTableProps {
    columns: ITableColumns;
    getId: Function;
    items: Array<any>;
    deleteItem: Function;
    updateItem: Function;
    createItem: Function;
    onCellClick?: Function;
}

export interface ICRUDTableState {
    editableRow: any | null;
    newRow: any | null;
}
