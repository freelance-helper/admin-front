import {axios} from 'libs/axios';
import {IApiResponse} from "react-app-env";

export class Api {
    static async get<FetchedData>(url: string, params?: any): Promise<IApiResponse<FetchedData>> {
        const res = await axios.get(url);

        return {
            status: res.status,
            data: res.data
        }
    }

    static async put<UpdatedData>(url: string, body?: any, params?: any): Promise<IApiResponse<UpdatedData>> {
        const res = await axios.put(url, body);

        return {
            status: res.status,
            data: res.data
        }
    }

    static async post<CreatedData>(url: string, body: any): Promise<IApiResponse<CreatedData>> {
        const res = await axios.post(url, body);

        return {
            status: res.status,
            data: res.data
        }
    }

    static async delete(url: string) {
        const res = await axios.delete(url);

        return {status: res.status};
    }
}
