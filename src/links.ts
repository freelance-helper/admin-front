import {home} from './pages/home';
import {ILink} from "./react-app-env";

export const links: ILink = {
    path: '/',
    children: {
        home
    }
};
