/// <reference types="react-scripts" />

export interface ILink {
    path: string;
    children?: {
        [key: string]: ILink;
    }
}

export interface IApiResponse<Data> {
    status: number,
    data: Data
}
