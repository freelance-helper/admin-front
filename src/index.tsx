import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from "react-router-dom";

import App from 'pages/app';
import {GlobalStyles} from 'ui/theme/normalize';
import {configureStore} from 'store';
import * as serviceWorker from './libs/service-worker';

const store = configureStore();

ReactDOM.render(
    <Fragment>
        <GlobalStyles/>
        <Provider store={store}>
            <Router>
                <App/>
            </Router>
        </Provider>
    </Fragment>,
    document.getElementById('root')
);

serviceWorker.unregister();
