export enum LoadingStatus {
    INITIAL = 'INITIAL',
    FETCHING = 'FETCHING',
    SUCCESS = 'SUCCESS',
    FAIL = 'FAIL',
}
