import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

import {exchangesReducer, exchangeDetailsReducer} from 'pages/home';

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const reducer = combineReducers({
    exchanges: exchangesReducer,
    exchangeDetails: exchangeDetailsReducer,
});

export function configureStore(initialState = {}) {
    const middlewares = [
        createLogger({ collapsed: true }),
        thunk
    ];

    return createStore(
        reducer,
        initialState,
        composeEnhancers(applyMiddleware(...middlewares))
    )
}
